<?php if(isset($_GET['status'])=='login_fail'):?>
    <?php if($_GET['status']=='login_fail'):?>
        <div class="box error" >
            La connexion a échoué.Vérifiez vos identifiants
        </div>

    <?php elseif($_GET['status']=='signin_succes'):?>
        <div class="box info">
            Inscription réussie! Vous pouvez dès à présent vous connecter.
        </div>
    <?php elseif($_GET['status']=='signin_fail'):?>
        <div class="box error">
            Echec d'innscription. Verifiez vos informations.
        </div>
    <?php elseif($_GET['status']=='logout_succes'):?>
        <div class="box info" id="msgBox">
            Vous êtes déconnecté. A bientôt !
        </div>
    <?php endif;?>
<?php endif;?>
<?php if(isset($_GET['deleteUser'])):?>
    <div class="box info">
        Votre compte a été supprimé avec succes Merci et Aurevoir
    </div>
<?php endif;?>
<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" id="formSignin" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input type="text" name="userlastname" placeholder="Nom" id="userlastname"/>

  <p>Prénom</p>
  <input type="text" name="userfirstname" placeholder="Prénom" id="userfirstname"/>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" id="useremail"/>

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" id="userpass"/>

  <p>Répéter le mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" id="userpassconf"/>

  <input type="submit" id=formSigninButton" value="Inscription" />

</form>

</div>
<script src="/public/scripts/signin.js"></script>
