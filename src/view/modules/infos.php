
<?php if(isset($_GET['statusUpdate']) AND $_GET['statusUpdate']=='fail'):?>
        <div class="box error" >
           Echec de mis à jour verifiez vos informations ou le mail que vous avez donneé existe deja
        </div>
    <?php elseif(isset($_GET['statusUpdate']) AND $_GET['statusUpdate']=='succes'):?>
        <div class="box info">
            Tes informations personnelles ont été mis à jour.
        </div>
    <?php endif;?>
<div id="account">
    <form class="account-signin" id="formSignin" method="post" action="/account/update">

        <h2>Informations du Compte</h2>
        <h3>Informations personnelles</h3>
        <p>Prénom</p>
        <input type="text" name="firstnameUpdate" placeholder="Prénom" id="userfirstname" value="<?=$_SESSION['userfirstname']?>"/>

        <p>Nom</p>
        <input type="text" name="lastnameUpdate" placeholder="Nom" id="userlastname" value="<?=$_SESSION['userlastname']?>"/>

        <p>Adresse mail</p>
        <input type="text" name="mailUpdate" placeholder="Adresse mail" id="useremail" value="<?=$_SESSION['usermail']?>"/>

        <input type="submit" id=formSigninButton" value="Modifier vos informations" />
      <div class="box error"><a href="/account/deleteAccount">Supprimer mon Compte</a></div>
    </form>
</div>

<script src="/public/scripts/signin.js"></script>