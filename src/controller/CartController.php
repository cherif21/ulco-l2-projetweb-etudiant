<?php


namespace controller;


class CartController
{
    //methode qui affiche le parnier de l'utilisateur
    public function cart(){
        if(!isset($_SESSION['cart'])){
            header("Location: /store" );
            exit();
        }
        $params=array(
            "title"=>"Cart",
            "module"=>"cart.php",
            "cart"=>$_SESSION['cart']
        );
        \view\Template::render($params);
    }
    //methode qui ajoute un produit dans le panier
    public function add(){
        if(!isset($_SESSION['cart'])){
            header("Location: /account" );
            exit();
        }
        var_dump($_POST);
        $id=htmlspecialchars($_POST['id']);
        $produitAdd=array(
            "image"=>htmlspecialchars($_POST['image']),
            "nom"=>htmlspecialchars($_POST['nom']),
            "categorie"=>htmlspecialchars($_POST['categorie']),
            "prix"=>htmlspecialchars($_POST['prix']),
            "quantite"=>htmlspecialchars($_POST['quantite'])
        );
        reset($_POST);
        if(array_key_exists($id,$_SESSION['cart'])) $_SESSION['cart'][$id]['quantite']+=$produitAdd['quantite'];
        else $_SESSION['cart'][$id]=$produitAdd;
        header("Location: /cart" );
        exit();
    }

    //methode qui permet de vider le panier
    public function cleanCart(){
        if(isset($_SESSION['cart'])){
            $_SESSION['cart']=array();
            header('Location: /cart');
            exit();
        }
        header('Location:/store');
        exit();
    }

    public function removeProductCart(int $idProduct){
        if(isset($_SESSION['cart'])){
            if(array_key_exists($idProduct,$_SESSION['cart'])){
                unset($_SESSION['cart'][$idProduct]);
                header('Location: /cart');
                exit();
            }
        }
        header('Location:/store');
        exit();
    }
}