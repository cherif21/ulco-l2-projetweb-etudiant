<?php


namespace model;


class AccountModel
{
    //methode qui va verifier les informations passées en parametres selon les contraintes
    public static function check(string $firstname,string $lastname,string $mail,string $password):bool{
        if(strlen($firstname)<2 OR strlen($lastname)<2) return false;
        if(!filter_var($mail,FILTER_VALIDATE_EMAIL)) return false;
        if(strlen($password)<6) return false;
        $db=\model\Model::connect();
        $verifMail=$db->prepare("SELECT mail FROM account WHERE mail=?");
        $verifMail->execute(array($mail));
        if($verifMail->fetch())return false;
        return true;
    }

    //methode qui inscrit l'utilisateur si les informations transmis sont correct
    public static function signin(string $firstname,string $lastname,string $mail,string $password):bool{
        if(self::check($firstname,$lastname,$mail,$password)){
            $db=\model\Model::connect();
            $sql="INSERT INTO account (firstname,lastname,mail,password) VALUES(?,?,?,?)";
            $req=$db->prepare($sql);
            if($req->execute(array($firstname,$lastname,$mail,password_hash($password,PASSWORD_DEFAULT))))return true;
        }
        return false;
    }

    //methode qui selectionne un utilisateur s'il existe dans la BD
    public static function login(string $mail,string $password){
        $db=\model\Model::connect();
        $sql="SELECT firstname,lastname,mail,password FROM account WHERE mail=?";
        $req=$db->prepare($sql);
        $req->execute(array($mail));
       $user=$req->fetch();
       if(password_verify($password,$user['password'])) return $user;
       return null;
    }

    //methode qui met a jour les infos du l'utilisateur dans la BD
    public static function updateInfos(array $tabInfo):bool{
        if(strlen($tabInfo['firstnameUpdate'])<2 OR strlen($tabInfo['lastnameUpdate'])<2) return false;
        if(!filter_var($tabInfo['mailUpdate'],FILTER_VALIDATE_EMAIL)) return false;
        $db=\model\Model::connect();
        if($tabInfo['mailUpdate']!=$tabInfo['mail']){
            $verifMail=$db->prepare("SELECT mail FROM account WHERE mail=?");
            $verifMail->execute(array($tabInfo['mailUpdate']));
            if($verifMail->fetch())return false;
        }
        $sql="UPDATE account SET firstname=?, lastname=?, mail=? WHERE mail=?";
        $req=$db->prepare($sql);
        if($req->execute(array($tabInfo['firstnameUpdate'],$tabInfo['lastnameUpdate'],$tabInfo['mailUpdate'],$tabInfo['mail']))) return true;
        return false;
    }

    //methode qui supprime le compte de l'utilisateur
    public static function deleteUser(string $mail):bool{
        $db=\model\Model::connect();
        $sql="DELETE FROM account WHERE mail=?";
        $req=$db->prepare($sql);
        if($req->execute(array($mail))) return true;
        return false;
    }
}