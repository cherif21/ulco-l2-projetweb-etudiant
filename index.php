<?php
session_start();
/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

// GET "/store"
$router->get('/store','controller\StoreController@store');

//GET "/store/id
$router->get('/store/{:num}','controller\StoreController@product');

//GET "/account"
$router->get('/account','controller\AccountController@account');

//POST "account/login"
$router->post('/account/login','controller\AccountController@login');

//POST "account/signin"
$router->post('/account/signin','controller\AccountController@signin');

//GET "/account/logout"
$router->get('/account/logout','controller\AccountController@logout');

//GET "/account/deleteAccount
$router->get('/account/deleteAccount','controller\AccountController@deleteAccount');

//POST "/postComment"
$router->post('/postComment/{:num}','controller\CommentController@postComment');

//POST "/store/search"
$router->post('/store/search','controller\StoreController@search');

//GET "/account/infos"
$router->get('/account/infos','controller\AccountController@infos');

//POST "/account/update"
$router->post('/account/update','controller\AccountController@update');

//GET "/cart"
$router->get('/cart','controller\CartController@cart');

//POST "/cart/add"
$router->post('/cart/add','controller\CartController@add');

//GET "/cart/cleanPanier"
$router->get('/cart/cleanCart','controller\CartController@cleanCart');

//GET "/cart/cleanPanier"
$router->get('/cart/removeProductCart/{:num}','controller\CartController@removeProductCart');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');


/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
